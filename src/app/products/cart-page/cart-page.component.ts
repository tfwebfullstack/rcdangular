import { Component, OnDestroy } from '@angular/core';
import { ProductPanier } from 'src/app/models/ProductPanier';
import { PanierService } from 'src/app/services/panier.service';
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnDestroy {
  listPanier : ProductPanier[] = [] 
  prixTotal : number = 0

  panierSub : Subscription
  prixTotSub : Subscription

  constructor(private _panierService : PanierService) {

    this.panierSub = this._panierService.panier$.subscribe((newPanier) => {
      console.log("ABONNEMENT CART PAGE ! Nouveau panier : ", newPanier );        
      this.listPanier = newPanier
    })

    this.prixTotSub = this._panierService.prixTotal$.subscribe((newTotal) => {
      this.prixTotal = newTotal
    })

  }

  //Appelé quand le composant est "détruit" -> Plus affiché sur la page
  ngOnDestroy(): void {
    console.log("DESTRUCTION DE LA PAGE : On se désabonne de tous les observables");    
    this.panierSub.unsubscribe()
    this.prixTotSub.unsubscribe()
  }
}
