import { Product } from "./Product";

export interface ProductPanier extends Product {
    //Comme il extends Product, un ProductPanier aura d'office un id, name, price
    quantity : number;
}